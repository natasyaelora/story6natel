
from django.urls import path
from app_story6 import views

urlpatterns = [
    path('', views.home, name='home'),
]