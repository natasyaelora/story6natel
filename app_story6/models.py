from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class Status(models.Model):
	status = models.TextField(max_length = 300)
	date = models.DateTimeField(auto_now_add = True, null = True)