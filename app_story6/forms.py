from django import forms
from django.forms import widgets
from . import models 
class StatusForms(forms.ModelForm):
	class Meta:
		model = models.Status
		fields = ['status']

	widgets = {
		'status' : forms.Textarea(attrs={'cols':10, 'rows': 10, "class" : "form-control"}),
	}