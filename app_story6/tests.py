from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from . import views
from .models import *
from .forms import *
from .views import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story6UnitTest(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home.html')

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.home)

	def test_home_contains_greeting(self):
		response = Client().get('/')
		response_content = response.content.decode('utf-8')
		self.assertIn("Hello! What's On Your Mind?", response_content)

	def test_single_entry(self):
		Status.objects.create(status='status')
		response = Client().get('/')
		self.assertContains(response, 'status')
		self.assertEqual(Status.objects.all().count(), 1)

	def test_model_created(self):
		Status.objects.create(status = "testing")
		status_obj = Status.objects.all()
		self.assertEqual(len(status_obj), 1)

		obj = status_obj[0]
		self.assertEqual(obj.status, "testing")

		self.assertEqual(obj.date.minute, timezone.now().minute)
		self.assertEqual(obj.date.hour, timezone.now().hour)
		self.assertEqual(obj.date.date(), timezone.now().date())

	def test_forms(self):
		form_data = {
		'status' : 'it is a status',
		}
		form = StatusForms(data = form_data)
		self.assertTrue(form.is_valid())
		request = Client().post('/', data = form_data)
		self.assertEqual(request.status_code, 302)

		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_valid_input(self):
		status_form = StatusForms({'status': "Status"})
		self.assertTrue(status_form.is_valid())
		status = Status()
		status.title = status_form.cleaned_data['status']
		status.save()
		self.assertEqual(status.title, "Status")

	def test_input_maximum_length_300(self):
		long_txt = ''
		for i in range(300):
			long_txt += 'b'

		form = StatusForms(data={'status': long_txt})
		self.assertEqual(len(form.data['status']), 300)

		post_resp = self.client.post('/', data={'status': long_txt})
		status_obj = Status.objects.get(id=1)
		self.assertEqual(len(status_obj.status), 300)

class Story6FunctionalTest(LiveServerTestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		chrome_options.add_argument('--dns-prefetch-disable')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Story6FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Story6FunctionalTest, self).tearDown()

	def test_post(self):
		selenium = self.browser
		selenium.get(self.live_server_url)
		new_status = selenium.find_element_by_id('id_status')
		new_status.send_keys("Coba Coba")
		#time.sleep(3)	
		submit = selenium.find_element_by_id('submit')
		submit.send_keys(Keys.RETURN)
		#time.sleep(3)
		selenium.get(self.live_server_url)
		#time.sleep(3)
		self.assertIn('Coba Coba', selenium.page_source)

