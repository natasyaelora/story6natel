from django.shortcuts import render, redirect
from . import forms
from .models import Status

# Create your views here.
def home(request):
	if request.method == "POST":
		form = forms.StatusForms(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/')
	else:
		form = forms.StatusForms()
	status = Status.objects.all()
	return render(request, 'home.html', {'form' : form , 'status' : status})